package libName

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var responseJSON = `{"data":"Nebula Lambda","env":"` + os.Getenv("ENV") + `"}`

func TestResponse(t *testing.T) {
	data, err := json.Marshal(GetResponse())
	if err != nil {
		t.Logf("Error: %s", err)
	} else {
		assert.Equal(t, responseJSON, string(data))
	}
}
