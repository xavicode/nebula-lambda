package libName

import "os"

type Response struct {
	Data string `json:"data"`
	Env  string `json:"env"`
}

func GetResponse() Response {
	return Response{Data: "Nebula Lambda Go", Env: os.Getenv("ENV")}
}
