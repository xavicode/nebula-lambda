import { Injectable } from '@nestjs/common';

@Injectable()
export class LibNameService {
	public response() {
		return { data: 'Nebula Lambda Nest.js (TypeScript)', env: process.env.ENV };
	}
}
