import { Module } from '@nestjs/common';
import { LibNameService } from './lib-name.service';

@Module({
	providers: [LibNameService],
	exports: [LibNameService],
})
export class LibNameModule {}
