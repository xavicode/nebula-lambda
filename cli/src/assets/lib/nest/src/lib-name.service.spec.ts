import { Test, TestingModule } from '@nestjs/testing';
import { LibNameService } from './lib-name.service';

describe('LibNameService', () => {
	let service: LibNameService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [LibNameService],
		}).compile();

		service = module.get<LibNameService>(LibNameService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
