import os

from . import response


def test_response():
    assert response() == {"data": "Nebula Lambda", "env": os.environ.get("ENV")}
