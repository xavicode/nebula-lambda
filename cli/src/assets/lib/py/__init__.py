import os


def response():
    return {"data": "Nebula Lambda Python", "env": os.environ.get("ENV")}
