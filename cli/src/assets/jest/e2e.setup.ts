import request from 'supertest';
import { faker } from '@faker-js/faker';

// @ts-ignore
global.host_path = 'https://api.com';

// @ts-ignore
global.request = (path: string) => {
	return {
		// @ts-ignore
		get: () => request(global.host_path).get(path),
		// @ts-ignore
		post: () => request(global.host_path).post(path),
		// @ts-ignore
		put: () => request(global.host_path).put(path),
		// @ts-ignore
		patch: () => request(global.host_path).patch(path),
		// @ts-ignore
		delete: () => request(global.host_path).delete(path),
	};
};

// @ts-ignore
global.faker = faker;
