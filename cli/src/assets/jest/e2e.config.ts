import { JestConfigWithTsJest } from 'ts-jest';

const config: JestConfigWithTsJest = {
	clearMocks: true,
	coverageProvider: 'v8',
	moduleFileExtensions: ['js', 'ts'],
	testTimeout: 1500000,
	testEnvironment: 'node',
	passWithNoTests: true,

	roots: ['<rootDir>/../../src'],

	testMatch: ['**/*.api.ts'],
	transform: {
		'^.+\\.ts$': ['ts-jest', { tsconfig: '<rootDir>/../../tsconfig.json' }],
	},

	setupFiles: ['<rootDir>/e2e.setup.ts'],
};

export default config;
