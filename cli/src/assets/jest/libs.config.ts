import { resolve } from 'path';
import { JestConfigWithTsJest } from 'ts-jest';
import { readdirSync, existsSync } from 'fs-extra';

const rootPath = resolve(process.cwd());

const getModuleNameMapper = () => {
	const mapper: { [k: string]: string } = {};
	if (existsSync(resolve(rootPath, 'libs'))) {
		for (const lib of readdirSync(resolve(rootPath, 'libs'))) {
			mapper[`^@app/${lib}(|/.*)$`] = `<rootDir>/libs/${lib}/src/$1`;
		}
		return mapper;
	}
};

const config: JestConfigWithTsJest = {
	moduleFileExtensions: ['js', 'json', 'ts'],
	rootDir: rootPath,
	testRegex: '.*\\.spec\\.ts$',
	transform: {
		'^.+\\.(t|j)s$': 'ts-jest',
	},
	collectCoverageFrom: ['**/*.(t|j)s'],
	coverageDirectory: './coverage',
	testEnvironment: 'node',
	roots: ['<rootDir>/libs/'],
	setupFiles: [resolve(rootPath, 'config', 'jest', 'libs.setup.ts')],
	moduleNameMapper: getModuleNameMapper(),
};

export default config;
