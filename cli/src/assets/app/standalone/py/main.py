import os
from typing import Any

from aws_lambda_typing.context import Context


def handler(event: Any, context: Context = None):
    return {"data": "Nebula Lambda Python", "env": os.environ.get("ENV")}
