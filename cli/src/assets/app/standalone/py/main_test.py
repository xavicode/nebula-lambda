import os

from main import handler


def test_handler():
    response = handler(dict({}))
    assert response == {"data": "Nebula Lambda Python", "env": os.environ.get("ENV")}
