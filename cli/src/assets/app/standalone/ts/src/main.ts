import middy from '@middy/core';
import { Context } from 'aws-lambda';

const main = async (event: any, context: Context) => {
	return { data: 'Nebula Lambda TypeScript', env: process.env.ENV };
};

export const handler = middy(main);
