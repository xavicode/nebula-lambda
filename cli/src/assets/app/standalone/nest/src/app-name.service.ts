import { Injectable } from '@nestjs/common';
import { Context } from 'aws-lambda';

@Injectable()
export class AppNameService {
	handler(event: any, context: Context) {
		return { data: 'Nebula Lambda Nest.js (TypeScript)', env: process.env.ENV };
	}
}
