import { Module } from '@nestjs/common';
import { AppNameService } from './app-name.service';

@Module({
	providers: [AppNameService],
	exports: [AppNameService],
})
export class AppNameModule {}
