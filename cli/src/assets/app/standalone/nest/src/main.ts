import { NestFactory } from '@nestjs/core';
import { Callback, Context, Handler } from 'aws-lambda';
import { AppNameModule } from './app-name.module';
import { AppNameService } from './app-name.service';

let service: AppNameService;

async function bootstrap() {
	const app = await NestFactory.createApplicationContext(AppNameModule);
	return app.get<AppNameService>(AppNameService);
}

export const handler: Handler = async (event: any, context: Context, callback: Callback) => {
	service = service ?? (await bootstrap());
	callback(null, service.handler(event, context));
};
