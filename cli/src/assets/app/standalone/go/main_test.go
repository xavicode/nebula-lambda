package main

import (
	"context"
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var responseJSON = `{"data":"Nebula Lambda Go","env":"` + os.Getenv("ENV") + `"}`

func TestRoot(t *testing.T) {
	res, e := Handler(context.TODO(), nil)

	// Assertions
	if assert.NoError(t, e) {
		resJSON, _ := json.Marshal(res)
		assert.Equal(t, responseJSON, string(resJSON))
	}
}
