package main

import (
	"context"
	"github.com/mefellows/vesper"
	"os"
)

type Response struct {
	Data string `json:"data"`
	Env  string `json:"env"`
}

func Handler(ctx context.Context, evt interface{}) (Response, error) {
	return Response{Data: "Nebula Lambda Go", Env: os.Getenv("ENV")}, nil
}

func main() {
	m := vesper.New(Handler)
	m.Start()
}
