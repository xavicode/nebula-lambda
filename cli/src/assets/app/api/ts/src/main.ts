import middy from '@middy/core';
import httpRouterHandler, { Route } from '@middy/http-router';
import httpHeaderNormalizer from '@middy/http-header-normalizer';
import { Context, APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

const get = middy().handler(async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
	return {
		statusCode: 200,
		body: JSON.stringify({ data: 'Nebula Lambda TypeScript', env: process.env.ENV }),
	};
});

const routes: Route<APIGatewayProxyEvent>[] = [
	{
		method: 'GET',
		path: '/app-name',
		handler: get,
	},
];

export const handler = middy().use(httpHeaderNormalizer()).handler(httpRouterHandler(routes));
