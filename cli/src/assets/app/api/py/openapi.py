import json
import os
import sys

sys.path.append(os.getcwd())

from fastapi.openapi.utils import get_openapi

from main import app


def openapi():
    openapi_path = os.path.join(os.getcwd(), 'docs', 'public', '_openapi')

    if not os.path.exists(openapi_path):
        os.mkdir(openapi_path)

    with open(os.path.join(openapi_path, 'app-name.json'), 'w') as f:
        json.dump(get_openapi(
            title=app.title,
            openapi_version='3.0.0',
            version=app.version,
            description=app.description,
            routes=app.routes,
        ), f)


if __name__ == '__main__':
    openapi()
