import os

from fastapi import APIRouter

router = APIRouter()


@router.get("/")
def root():
    return {"data": "Nebula Lambda Python", "env": os.environ.get("ENV")}
