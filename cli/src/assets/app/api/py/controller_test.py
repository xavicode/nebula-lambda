import os

from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_root_controller():
    response = client.get("/app-name")
    assert response.status_code == 200
    assert response.json() == {"data": "Nebula Lambda Python", "env": os.environ.get("ENV")}
