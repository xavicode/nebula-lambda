package main

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"os"
)

type Response struct {
	Data string `json:"data"`
	Env  string `json:"env"`
}

func Root(c echo.Context) error {
	return c.JSON(http.StatusOK, Response{Data: "Nebula Lambda Go", Env: os.Getenv("ENV")})
}

func Controller(router *echo.Group) {
	router.GET("/", Root)
}
