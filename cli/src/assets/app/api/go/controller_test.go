package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

var responseJSON = `{"data":"Nebula Lambda Go","env":"` + os.Getenv("ENV") + `"}` + "\n"

func TestRoot(t *testing.T) {
	// Setup
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", strings.NewReader(""))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	// Assertions
	if assert.NoError(t, Root(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, responseJSON, rec.Body.String())
	}
}
