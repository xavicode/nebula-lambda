import { NestFactory } from '@nestjs/core';
import { configure as serverlessExpress } from '@vendia/serverless-express';
import { Callback, Context, Handler } from 'aws-lambda';
import { AppNameModule } from './app-name.module';

let server: Handler;

async function bootstrap(): Promise<Handler> {
	const app = await NestFactory.create(AppNameModule);
	app.setGlobalPrefix('app-name');
	app.enableCors();

	await app.init();

	return serverlessExpress({ app: app.getHttpAdapter().getInstance() });
}

export const handler: Handler = async (event: any, context: Context, callback: Callback) => {
	server = server ?? (await bootstrap());
	return server(event, context, callback);
};
