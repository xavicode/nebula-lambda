import { Test, TestingModule } from '@nestjs/testing';
import { AppNameController } from './app-name.controller';
import { AppNameService } from './app-name.service';

describe('AppNameController', () => {
	let appNameController: AppNameController;

	beforeEach(async () => {
		const app: TestingModule = await Test.createTestingModule({
			controllers: [AppNameController],
			providers: [AppNameService],
		}).compile();

		appNameController = app.get<AppNameController>(AppNameController);
	});

	describe('root', () => {
		it('should return "Nebula Lambda Nest.js (TypeScript)"', () => {
			const resp = appNameController.root();
			expect(resp.data).toBe('Nebula Lambda Nest.js (TypeScript)');
		});
	});
});
