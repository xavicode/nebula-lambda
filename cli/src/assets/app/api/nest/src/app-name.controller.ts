import { Controller, Get } from '@nestjs/common';
import { AppNameService } from './app-name.service';

@Controller()
export class AppNameController {
	constructor(private readonly appNameService: AppNameService) {}

	@Get()
	public root() {
		return this.appNameService.root();
	}
}
