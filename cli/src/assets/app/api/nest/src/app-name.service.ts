import { Injectable } from '@nestjs/common';

@Injectable()
export class AppNameService {
	public root() {
		return { data: 'Nebula Lambda Nest.js (TypeScript)', env: process.env.ENV };
	}
}
