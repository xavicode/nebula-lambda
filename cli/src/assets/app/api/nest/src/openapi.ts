import { resolve } from 'path';
import { existsSync, mkdirpSync, writeJSONSync } from 'fs-extra';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppNameModule } from './app-name.module';

async function openapi() {
	const app = await NestFactory.create(AppNameModule);

	app.setGlobalPrefix('app-name');

	const config = new DocumentBuilder().build();

	const document = SwaggerModule.createDocument(app, config);

	const openapiDir = resolve(process.cwd(), 'docs', 'public', '_openapi');

	if (!existsSync(openapiDir)) {
		mkdirpSync(openapiDir);
	}

	writeJSONSync(resolve(openapiDir, 'app-name.json'), document);
}

openapi();
