import { platform } from 'process';
import { execSync } from 'child_process';

export function checkBin(bin: string) {
	let res: string;
	try {
		res = execSync((platform === 'win32' ? 'where' : 'which ') + bin, { encoding: 'utf-8' });
	} catch (e: any) {
		res = e.toString();
	}
	return !(res.search('Error: Command failed: ' + (platform === 'win32' ? 'where' : 'which ')) >= 0);
}

export function getVersion(bin: string, cmd = '--version') {
	return execSync(bin + ' ' + cmd, { encoding: 'utf-8' }).replace('\n', '');
}
