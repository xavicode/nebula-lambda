export function toKebabCase(word: string) {
	return word
		.split(' ')
		.map((s) => s.toLowerCase())
		.join('-');
}

export function toSnakeCase(word: string) {
	return toKebabCase(word).split('-').join('_');
}

export function toCamelCase(word: string) {
	return toKebabCase(word)
		.split('-')
		.map((s, i) => (i ? s.charAt(0).toUpperCase() + s.slice(1) : s))
		.join('');
}

export function toPascalCase(word: string) {
	return toKebabCase(word)
		.split('-')
		.map((s) => s.charAt(0).toUpperCase() + s.slice(1))
		.join('');
}

export function toCapitalize(word: string) {
	return toKebabCase(word)
		.split('-')
		.map((s) => s.charAt(0).toUpperCase() + s.slice(1))
		.join('');
}
