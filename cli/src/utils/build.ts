import { copySync, existsSync, mkdirpSync, readdirSync, readFileSync, rmSync } from 'fs-extra';
import { resolve } from 'path';
import { rootPath } from './const';
import { rimrafSync } from 'rimraf';
import { getConfigApp, App } from './config';
import { red, yellow } from 'colors/safe';
import { execSync } from 'child_process';

export async function build(appName?: string) {
	if (appName) {
		await buildApp(appName.split(' ').join('-'));
	} else {
		if (existsSync(resolve(rootPath, 'apps'))) {
			const apps = readdirSync(resolve(rootPath, 'apps'));
			if (apps.length) {
				for (const appName of apps) {
					await buildApp(appName);
				}
			} else {
				console.log(yellow('Alert: No apps'));
			}
		} else {
			console.log(yellow('Alert: No apps'));
		}
	}
}

export async function buildApp(appName: string) {
	if (existsSync(resolve(rootPath, 'apps', appName))) {
		if (existsSync(resolve(rootPath, 'dist', 'apps', appName))) {
			rimrafSync(resolve(rootPath, 'dist', 'apps', appName));
		}
		const app = getConfigApp(appName);

		if (app) {
			if (app.PackageType === 'docker') {
				await buildDocker(app, appName);
			} else {
				if (app.Language === 'nest') {
					await buildNest(appName);
				} else if (app.Language === 'ts') {
					await buildTs(appName);
				} else if (app.Language === 'py') {
					await buildPy(appName);
				} else if (app.Language === 'go') {
					await buildGo(appName);
				} else {
					console.log(red(`Error: Application "${appName}" no exists in this project.`));
				}
			}
		}
	} else {
		console.log(red(`Error: Application "${appName}" no exists in this project.`));
	}
}

export async function buildNest(appName: string) {
	execSync(`nest build ${appName} --config ${resolve(rootPath, 'config', 'nest-cli.json')}`, {
		cwd: rootPath,
		stdio: 'inherit',
	});
}

export async function buildTs(appName: string) {
	execSync(`webpack build --config ./config/webpack.config.js --env AppName=${appName}`, {
		cwd: rootPath,
		stdio: 'inherit',
	});
}

export async function buildPy(appName: string) {
	if (!existsSync(resolve(rootPath, '.venv')) || !process.env.VIRTUAL_ENV) {
		execSync('poetry install', {
			cwd: rootPath,
		});
		console.log(red('Error: Python virtual environment not activated'));
		return;
	}

	const appPath = resolve(rootPath, 'apps', appName);

	const compilePath = resolve(rootPath, 'dist', 'apps', appName);

	copySync(appPath, compilePath);
	rimrafSync(resolve(compilePath, '.mypy_cache'));
	rmSync(resolve(compilePath, 'template.yaml'));

	for (const file of readdirSync(compilePath)) {
		if (file.endsWith('.py')) {
			const python_file = resolve(compilePath, file);
			const python_file_content = readFileSync(python_file, 'utf-8');
			for (const reg of [
				new RegExp('\nimport modules.(.*?)\n', 'g'),
				new RegExp('from modules.(.*?) import', 'g'),
				new RegExp('from modules import (.*?)\n', 'g'),
			]) {
				for (const dependency of Array.from(python_file_content.matchAll(reg))) {
					const libName = dependency[1].split('.')[0];
					if (!existsSync(resolve(compilePath, 'modules'))) {
						mkdirpSync(resolve(compilePath, 'modules'));
					}
					copySync(resolve(rootPath, 'modules', libName), resolve(compilePath, 'modules', libName));
				}
			}
		}
	}

	execSync('pipreqs --force', {
		cwd: compilePath,
		stdio: 'inherit',
	});
}

export async function buildGo(appName: string) {
	execSync(
		`docker run --platform linux/amd64 --rm -u root -v ".:/asset-input:delegated" -v "./dist/apps/${appName}:/asset-output:delegated" --env "CGO_ENABLED=0" --env "GOOS=linux" --env "GOARCH=amd64" -w "/asset-input/apps/${appName}" "public.ecr.aws/sam/build-go1.x" bash -c "go build -o /asset-output/main"`,
		{
			cwd: rootPath,
			stdio: 'inherit',
		},
	);
}

export async function buildDocker(app: App, appName: string) {
	if (app.ImageRepository) {
		const appRoot = `./dist/apps/${appName}`;
		if (app.Language === 'nest' || app.Language === 'ts') {
			if (!existsSync(resolve(rootPath, appRoot))) {
				mkdirpSync(resolve(rootPath, appRoot));
			}
			mkdirpSync(resolve(rootPath, appRoot, 'apps'));
			copySync(resolve(rootPath, 'apps', appName), resolve(rootPath, appRoot, 'apps', appName));
			if (existsSync(resolve(rootPath, 'libs'))) {
				copySync(resolve(rootPath, 'libs'), resolve(rootPath, appRoot, 'libs'));
			}
			copySync(resolve(rootPath, 'package.json'), resolve(rootPath, appRoot, 'package.json'));
			copySync(resolve(rootPath, 'pnpm-lock.yaml'), resolve(rootPath, appRoot, 'pnpm-lock.yaml'));
			copySync(resolve(rootPath, 'tsconfig.json'), resolve(rootPath, appRoot, 'tsconfig.json'));
			if (app.Language === 'nest') {
				copySync(resolve(rootPath, 'config', 'nest-cli.json'), resolve(rootPath, appRoot, 'nest-cli.json'));
				mkdirpSync(resolve(rootPath, appRoot, 'config'));
				copySync(
					resolve(rootPath, 'config', 'webpack.nest.js'),
					resolve(rootPath, appRoot, 'config', 'webpack.nest.js'),
				);
			} else {
				copySync(
					resolve(rootPath, 'config', 'webpack.config.js'),
					resolve(rootPath, appRoot, 'config', 'webpack.config.js'),
				);
			}
		} else if (app.Language === 'py') {
			await buildPy(appName);
		} else if (app.Language === 'go') {
			await buildGo(appName);
		}
		const t1 = app.ImageRepository;
		const t2 = app.ImageTag && app.ImageTag !== 'latest' ? `${app.ImageRepository}:${app.ImageTag}` : '';
		const t = t2 ? `-t ${t1} -t ${t2}` : `-t ${t1}`;
		const build_arg = `--build-arg APP_NAME=${appName} --build-arg APP_ROOT=${appRoot} --build-arg APP_CMD=${app.Resource.Properties.Handler}`;
		execSync(
			`docker build --platform linux/amd64 --target app ${build_arg} ${t} -f ./apps/${appName}/Dockerfile .`,
			{
				cwd: rootPath,
				stdio: 'inherit',
			},
		);
		execSync(`docker push ${t1}`, {
			cwd: rootPath,
			stdio: 'inherit',
		});
		if (t2) {
			execSync(`docker push ${t2}`, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	} else {
		console.log(red(`Error: Application "${appName}" does not have an image repository.`));
	}
}
