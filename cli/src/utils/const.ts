import { resolve } from 'path';

export const rootPath = process.cwd();
export const envsPath = resolve(rootPath, 'dist', 'nebula-envs.json');
