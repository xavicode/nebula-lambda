import { resolve } from 'path';
import { existsSync, readdirSync, readFileSync, readJSONSync, writeFileSync, writeJSONSync } from 'fs-extra';
import { dump, load, DEFAULT_SCHEMA, Type } from 'js-yaml';

import { rootPath } from './const';
import * as cdk from 'aws-cdk-lib';
import { plainToInstance } from 'class-transformer';
import { toKebabCase, toPascalCase } from './strings';

export interface Event {
	Type: string;
	Properties: Record<string, string>;
}

export interface Resource {
	Type: string;
	Properties: { [k: string]: any };
	Metadata?: { [k: string]: string };
}

export interface ResourceEventProperties {
	FunctionName: string;
	CodeUri?: string;
	Handler: string;
	Runtime: string;
	Architectures: string[];
	Environment: {
		Variables: { [k: string]: string };
	};
	Events: { [k: string]: Event };
}

export interface ResourceEvent {
	Type: string;
	Properties: ResourceEventProperties;
	Metadata?: { [k: string]: string };
}

export interface Output {
	Description: string;
	Value: any;
}

interface Tags {
	sequence: { [p: string]: Type };
	scalar: { [p: string]: Type };
	mapping: { [p: string]: Type };
}

export const customTags: Tags = { sequence: {}, scalar: {}, mapping: {} };

const DocumentSchema = DEFAULT_SCHEMA.extend(
	(() => {
		function Model(name: string) {
			return class {
				constructor(data: string) {
					this.class = name;
					this.name = name;
					this.data = data;
				}

				public class: string;
				public name: string;
				public data: string;
			};
		}

		function CustomYamlType(name: string, kind: 'sequence' | 'scalar' | 'mapping') {
			const model = Model(name);
			return new Type('!' + name, {
				kind: kind,
				instanceOf: model,
				construct: function (data) {
					return new model(data);
				},
				represent: function (ref: any) {
					return ref.data;
				},
			});
		}

		const tags = readJSONSync(resolve(__dirname, '..', '..', 'src', 'assets', 'tags.json'));

		const cloudformationTags = [];

		for (const tag of tags['mapping']) {
			const t = CustomYamlType(tag, 'mapping');
			customTags.mapping[tag] = t;
			cloudformationTags.push(t);
		}

		for (const tag of tags['scalar']) {
			const t = CustomYamlType(tag, 'scalar');
			customTags.scalar[tag] = t;
			cloudformationTags.push(t);
		}

		for (const tag of tags['sequence']) {
			const t = CustomYamlType(tag, 'sequence');
			customTags.sequence[tag] = t;
			cloudformationTags.push(t);
		}

		return cloudformationTags;
	})(),
);

export const getDocument = <T>(data: T): string => {
	return dump(data, { indent: 4, lineWidth: -1, schema: DocumentSchema });
};

export interface App {
	Language: 'nest' | 'ts' | 'py' | 'go';
	PackageType: 'zip' | 'docker';
	ImageRepository?: string;
	ImageTag?: string;
	Name: string;
	Resource: ResourceEvent;
	Outputs: {
		[k: string]: Output;
	};
}

export function setConfigApp(app: App) {
	writeFileSync(resolve(rootPath, 'apps', app.Name, 'template.yaml'), getDocument<App>(app));
}

export function getConfigApp(appName: string, lang?: 'nest' | 'ts' | 'py' | 'go'): App | undefined {
	if (existsSync(resolve(rootPath, 'apps', appName, 'template.yaml'))) {
		return load(readFileSync(resolve(rootPath, 'apps', appName, 'template.yaml'), { encoding: 'utf-8' }), {
			schema: DocumentSchema,
			json: true,
		}) as App;
	} else if (lang) {
		const app: App = {
			Name: appName,
			Language: lang,
			PackageType: 'zip',
			Resource: {
				Type: 'AWS::Serverless::Function',
				Properties: {
					FunctionName: appName,
					Handler: '',
					Runtime: '',
					Architectures: ['x86_64'],
					Environment: {
						Variables: {},
					},
					Events: {},
				},
			},
			Outputs: {},
		};

		setConfigApp(app);
		return app;
	} else {
		return undefined;
	}
}

export function getApps() {
	if (existsSync(resolve(rootPath, 'apps'))) {
		return readdirSync(resolve(rootPath, 'apps'));
	} else {
		return [];
	}
}

export function getAppsPath() {
	return getApps().map((appName) => resolve(rootPath, 'apps', appName));
}

export function getAppsConfig(): App[] {
	return getApps()
		.map((appName) => getConfigApp(appName))
		.filter((config) => !!config) as App[];
}

class Config {
	public runtimes!: {
		ts: boolean;
		py: boolean;
		go: boolean;
		nest: boolean;
	};
	public stage!: 'dev' | 'test' | 'prod';
	public data!: Record<string, any>;

	private name!: string;
	private env!: cdk.Environment & { profile: string };

	get nameApp() {
		return toKebabCase(`${this.name}-${this.stage}`);
	}

	get profileEnv() {
		return this.env.profile;
	}

	get environment(): cdk.Environment {
		const env: { region?: string; account?: string } = {};
		if (this.env.region) {
			env.region = this.env.region;
		}
		if (this.env.account && this.env.account !== '000000000000') {
			env.account = this.env.account;
		}
		return env;
	}

	public getResourceName(s: string) {
		return toKebabCase(`${this.name}-${s}-${this.stage}`);
	}

	public getResourceId(s: string) {
		return toPascalCase(`${this.name}-${s}-${this.stage}`);
	}

	public getResourceOutput(s: string) {
		return `${this.getResourceId(s)}Output`;
	}
}

let config: Config;

export function getConfig() {
	if (!config) {
		config = plainToInstance(Config, readJSONSync(resolve(rootPath, 'config', 'nebula.config.json')));
	}
	return config;
}

export function setConfig(config: Config) {
	writeJSONSync(resolve(rootPath, 'config', 'nebula.config.json'), config);
}
