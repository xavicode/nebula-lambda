import { Command, Flags } from '@oclif/core';
import { resolve } from 'path';
import { execSync } from 'child_process';
import { blue, yellow } from 'colors/safe';
import { existsSync } from 'fs-extra';

import { rootPath } from '../utils/const';

export default class Format extends Command {
	static description = 'Format code';

	static examples = [
		'<%= config.bin %> <%= command.id %>',
		'<%= config.bin %> <%= command.id %> --ts',
		'<%= config.bin %> <%= command.id %> --py',
		'<%= config.bin %> <%= command.id %> --go',
	];

	static flags = {
		ts: Flags.boolean({
			description: 'TypeScript application language',
		}),
		py: Flags.boolean({
			description: 'Python application language',
		}),
		go: Flags.boolean({
			description: 'Go application language',
		}),
	};

	static args = {};

	public async run(): Promise<void> {
		const { flags } = await this.parse(Format);

		if (flags.ts || flags.py || flags.go) {
			if (flags.ts) {
				await this.formatTs();
			}
			if (flags.py) {
				await this.formatPy();
			}
			if (flags.go) {
				await this.formatGo();
			}
		} else {
			await this.formatTs();
			await this.formatPy();
			await this.formatGo();
		}
	}

	private formatTs() {
		this.log(blue('--- prettier output'));
		// prettier-ignore
		execSync("prettier --write --ignore-unknown --no-error-on-unmatched-pattern '{**/*,*}.{js,ts,jsx,tsx,json}'", {
			cwd: rootPath,
			stdio: "inherit"
		});
	}

	private formatPy() {
		if (existsSync(resolve(rootPath, '.venv')) || process.env.VIRTUAL_ENV) {
			if (existsSync(resolve(rootPath, 'apps')) || existsSync(resolve(rootPath, 'modules'))) {
				this.log(blue('--- black output'));
				if (existsSync(resolve(rootPath, 'apps'))) {
					execSync('./.venv/bin/black apps', {
						cwd: rootPath,
						stdio: 'inherit',
					});
				}
				if (existsSync(resolve(rootPath, 'modules'))) {
					execSync('./.venv/bin/black modules', {
						cwd: rootPath,
						stdio: 'inherit',
					});
				}
			}
			if (existsSync(resolve(rootPath, 'apps')) || existsSync(resolve(rootPath, 'modules'))) {
				this.log(blue('--- isort output'));
				if (existsSync(resolve(rootPath, 'apps'))) {
					execSync('./.venv/bin/isort apps', {
						cwd: rootPath,
						stdio: 'inherit',
					});
				}
				if (existsSync(resolve(rootPath, 'modules'))) {
					execSync('./.venv/bin/isort modules', {
						cwd: rootPath,
						stdio: 'inherit',
					});
				}
			}
		} else {
			this.log(yellow('Alert: Python virtual environment not activated'));
		}
	}

	private formatGo() {
		if (existsSync(resolve(rootPath, 'apps')) || existsSync(resolve(rootPath, 'packages'))) {
			this.log(blue('--- gofmt output'));
			if (existsSync(resolve(rootPath, 'apps'))) {
				execSync('gofmt -w ./apps', {
					cwd: rootPath,
					stdio: 'inherit',
				});
			}
			if (existsSync(resolve(rootPath, 'packages'))) {
				execSync('gofmt -w ./packages', {
					cwd: rootPath,
					stdio: 'inherit',
				});
			}
		}
	}
}
