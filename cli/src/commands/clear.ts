import { Command } from '@oclif/core';
import { resolve } from 'path';
import { rimrafSync } from 'rimraf';

import { rootPath } from '../utils/const';

export default class Clear extends Command {
	static description = 'Clear project';

	static examples = ['<%= config.bin %> <%= command.id %>'];

	static flags = {};

	static args = {};

	public async run(): Promise<void> {
		const paths = [
			'node_modules',
			'dist',
			'.venv',
			'.mypy_cache',
			'.pytest_cache',
			'vendor',
			'cli/node_modules',
			'cli/dist',
		].map((d) => resolve(rootPath, d));

		rimrafSync(paths);
	}
}
