import { Command, Flags } from '@oclif/core';
import { execSync } from 'child_process';
import { blue, red } from 'colors/safe';
import { existsSync, readdirSync } from 'fs-extra';
import { join, resolve } from 'path';
import { getConfigApp } from '../utils/config';

import { rootPath } from '../utils/const';

export default class Lint extends Command {
	static description = 'Lint code';

	static examples = [
		'<%= config.bin %> <%= command.id %>',
		'<%= config.bin %> <%= command.id %> --ts',
		'<%= config.bin %> <%= command.id %> --py',
		'<%= config.bin %> <%= command.id %> --go',
	];

	static flags = {
		ts: Flags.boolean({
			description: 'TypeScript application language',
		}),
		py: Flags.boolean({
			description: 'Python application language',
		}),
		go: Flags.boolean({
			description: 'Go application language',
		}),
	};

	static args = {};

	public async run(): Promise<void> {
		const { flags } = await this.parse(Lint);

		if (flags.ts || flags.py || flags.go) {
			if (flags.ts) {
				await this.lintTs();
			}
			if (flags.py) {
				await this.lintPy();
			}
			if (flags.go) {
				await this.lintGo();
			}
		} else {
			await this.lintTs();
			await this.lintPy();
			await this.lintGo();
		}
	}

	private lintTs() {
		this.log(blue('--- eslint output'));
		// prettier-ignore
		execSync('eslint --no-error-on-unmatched-pattern \'{**/*,*}.{js,ts,jsx,tsx}\'', {
			cwd: rootPath,
			stdio: 'inherit',
		});
	}

	private lintPy() {
		if (existsSync(join(rootPath, '.venv')) || process.env.VIRTUAL_ENV) {
			const rcfile = resolve(rootPath, 'config', '.pylintrc');

			const apps = readdirSync(resolve(rootPath, 'apps'));
			this.log(blue('--- mypy apps output'));
			for (const appName of apps) {
				const app = getConfigApp(appName);
				if (app && app.Language === 'py') {
					execSync(resolve(rootPath, '.venv', 'bin', 'mypy') + ' .', {
						cwd: resolve(rootPath, 'apps', appName),
						stdio: 'inherit',
					});
				}
			}
			this.log(blue('--- pylint apps output'));
			for (const appName of apps) {
				const app = getConfigApp(appName);
				if (app && app.Language === 'py') {
					execSync(`${resolve(rootPath, '.venv', 'bin', 'pylint')}  --rcfile ${rcfile} *.py`, {
						cwd: resolve(rootPath, 'apps', appName),
						stdio: 'inherit',
					});
				}
			}

			const libs = readdirSync(resolve(rootPath, 'libs'));
			this.log(blue('--- mypy libs output'));
			for (const libName of libs) {
				if (existsSync(resolve(rootPath, 'libs', libName, 'main.go'))) {
					execSync(resolve(rootPath, '.venv', 'bin', 'mypy') + ' .', {
						cwd: resolve(rootPath, 'apps', libName),
						stdio: 'inherit',
					});
				}
			}
			this.log(blue('--- pylint libs output'));
			for (const libName of libs) {
				if (existsSync(resolve(rootPath, 'libs', libName, 'main.go'))) {
					execSync(`${resolve(rootPath, '.venv', 'bin', 'pylint')}  --rcfile ${rcfile} *.py`, {
						cwd: resolve(rootPath, 'apps', libName),
						stdio: 'inherit',
					});
				}
			}
		} else {
			this.log(red('Error: Python virtual environment not activated'));
		}
	}

	private lintGo() {
		this.log(blue('--- golangci-lint output'));
		execSync('golangci-lint cache clean && go clean -modcache -cache -i && golangci-lint run', {
			cwd: rootPath,
			stdio: 'inherit',
		});
	}
}
