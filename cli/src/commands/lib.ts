import { resolve } from 'path';
import {
	copyFileSync,
	existsSync,
	mkdirpSync,
	readFileSync,
	readJSONSync,
	writeFileSync,
	writeJSONSync,
} from 'fs-extra';
import { Args, Command, Flags } from '@oclif/core';
import { execSync } from 'child_process';
import { blue, red } from 'colors/safe';
import { toCamelCase, toKebabCase, toPascalCase, toSnakeCase } from '../utils/strings';

import { rootPath } from '../utils/const';

export default class Lib extends Command {
	static description = 'Generate a new library (package) for apps in this project';

	static examples = [
		'<%= config.bin %> <%= command.id %> <name> --nest',
		'<%= config.bin %> <%= command.id %> <name> --ts',
		'<%= config.bin %> <%= command.id %> <name> --py',
		'<%= config.bin %> <%= command.id %> <name> --go',
	];

	static flags = {
		nest: Flags.boolean({
			description: 'Nest.js (TypeScript) application',
		}),
		ts: Flags.boolean({
			description: 'TypeScript library (package)',
		}),
		py: Flags.boolean({
			description: 'Python library (package)',
		}),
		go: Flags.boolean({
			description: 'Go library (package)',
		}),
	};

	static args = {
		name: Args.string({ description: 'Library (package) name' }),
	};

	public async run(): Promise<void> {
		const { args, flags } = await this.parse(Lib);

		if (args.name) {
			if (
				(flags.nest && flags.ts && flags.py && flags.go) ||
				(flags.nest && flags.ts) ||
				(flags.nest && flags.py) ||
				(flags.nest && flags.go) ||
				(flags.ts && flags.py) ||
				(flags.ts && flags.go) ||
				(flags.py && flags.go)
			) {
				this.log(red('Error: Just enter an library (package) language (runtime)'));
			} else {
				const libs_root = flags.ts ? 'libs' : flags.py ? 'modules' : 'packages';
				if (flags.nest || flags.ts || flags.py || flags.go) {
					if (!existsSync(resolve(rootPath, libs_root))) {
						mkdirpSync(resolve(rootPath, libs_root));
					}
					const libName = flags.py ? toSnakeCase(args.name) : toKebabCase(args.name);
					if (
						existsSync(resolve(rootPath, 'apps', libName)) ||
						existsSync(resolve(rootPath, libs_root, libName))
					) {
						this.log(red(`Error: Application/Library "${libName}" exists in this project already.`));
					} else {
						if (flags.nest) {
							await this.nest(libName);
						} else if (flags.ts) {
							await this.ts(libName);
						} else if (flags.py) {
							await this.py(libName);
						} else if (flags.go) {
							await this.go(libName);
						}

						if (
							existsSync(resolve(rootPath, '.git')) &&
							existsSync(resolve(rootPath, libs_root, libName))
						) {
							execSync(`git add ${resolve(rootPath, libs_root, libName)}/*`, { cwd: rootPath });
						}

						execSync('npm run format', { cwd: rootPath, stdio: 'inherit' });
					}
				} else {
					// prettier-ignore
					this.log(red('Error: Can\'t continue without an language for this library (package)'));
				}
			}
		} else {
			// prettier-ignore
			this.log(red('Error: Can\'t continue without an library name'));
		}
	}

	private async nest(libName: string) {
		this.log(blue('--- Nest.js (Typescript) library (package)'));

		const srcPath = resolve(rootPath, 'libs', libName);
		const assetPath = resolve(__dirname, '..', '..', 'src', 'assets', 'lib', 'nest');

		mkdirpSync(srcPath);
		mkdirpSync(resolve(srcPath, 'src'));

		writeFileSync(
			resolve(srcPath, 'tsconfig.lib.json'),
			readFileSync(resolve(assetPath, 'tsconfig.lib.json'), {
				encoding: 'utf-8',
			}).replace('lib-name', libName),
		);

		const kebabLibName = libName;
		const camelLibName = toCamelCase(libName);
		const pascalLibName = toPascalCase(libName);

		const remplaceLibName = (p: string) => {
			return readFileSync(p, {
				encoding: 'utf-8',
			})
				.split('lib-name')
				.join(kebabLibName)
				.split('libName')
				.join(camelLibName)
				.split('LibName')
				.join(pascalLibName);
		};

		writeFileSync(resolve(srcPath, 'src', 'index.ts'), remplaceLibName(resolve(assetPath, 'src', 'index.ts')));
		writeFileSync(
			resolve(srcPath, 'src', libName + '.service.spec.ts'),
			remplaceLibName(resolve(assetPath, 'src', 'lib-name.service.spec.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', libName + '.service.ts'),
			remplaceLibName(resolve(assetPath, 'src', 'lib-name.service.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', libName + '.module.ts'),
			remplaceLibName(resolve(assetPath, 'src', 'lib-name.module.ts')),
		);

		// nest-cli.json
		const nest = readJSONSync(resolve(rootPath, 'config', 'nest-cli.json'));

		nest['projects'][libName] = {
			type: 'library',
			root: 'libs/' + libName,
			entryFile: 'index',
			sourceRoot: 'libs/' + libName + '/src',
			compilerOptions: {
				tsConfigPath: 'libs/' + libName + '/tsconfig.lib.json',
			},
		};

		writeJSONSync(resolve(rootPath, 'config', 'nest-cli.json'), nest);

		// tsconfig.json
		const tsconfig = readJSONSync(resolve(rootPath, 'tsconfig.json'));

		tsconfig['compilerOptions']['paths']['@app/' + libName] = ['libs/' + libName + '/src'];
		tsconfig['compilerOptions']['paths']['@app/' + libName + '/*'] = ['libs/' + libName + '/src/*'];

		writeJSONSync(resolve(rootPath, 'tsconfig.json'), tsconfig);
	}

	private async ts(libName: string) {
		this.log(blue('--- Typescript library (package)'));

		const srcPath = resolve(rootPath, 'libs', libName);
		const assetPath = resolve(__dirname, '..', '..', 'src', 'assets', 'lib', 'ts');

		mkdirpSync(srcPath);
		mkdirpSync(resolve(srcPath, 'src'));

		writeFileSync(
			resolve(srcPath, 'tsconfig.lib.json'),
			readFileSync(resolve(assetPath, 'tsconfig.lib.json'), {
				encoding: 'utf-8',
			}).replace('lib-name', libName),
		);

		const kebabLibName = libName;
		const camelLibName = toCamelCase(libName);
		const pascalLibName = toPascalCase(libName);

		const remplaceLibName = (p: string) => {
			return readFileSync(p, {
				encoding: 'utf-8',
			})
				.split('lib-name')
				.join(kebabLibName)
				.split('libName')
				.join(camelLibName)
				.split('LibName')
				.join(pascalLibName);
		};

		writeFileSync(resolve(srcPath, 'src', 'index.ts'), remplaceLibName(resolve(assetPath, 'src', 'index.ts')));

		// tsconfig.json
		const tsconfig = readJSONSync(resolve(rootPath, 'tsconfig.json'));

		tsconfig['compilerOptions']['paths']['@app/' + libName] = ['libs/' + libName + '/src'];
		tsconfig['compilerOptions']['paths']['@app/' + libName + '/*'] = ['libs/' + libName + '/src/*'];

		writeJSONSync(resolve(rootPath, 'tsconfig.json'), tsconfig);
	}

	private async py(libName: string) {
		this.log(blue('--- Python library (package)'));

		const srcPath = resolve(rootPath, 'modules', libName);
		const assetPath = resolve(__dirname, '..', '..', 'src', 'assets', 'lib', 'py');

		mkdirpSync(srcPath);

		copyFileSync(resolve(assetPath, '__init__.py'), resolve(srcPath, '__init__.py'));
		copyFileSync(resolve(assetPath, 'main_test.py'), resolve(srcPath, 'main_test.py'));
	}

	private async go(libName: string) {
		this.log(blue('--- Go library (package)'));

		const srcPath = resolve(rootPath, 'packages', libName);
		const assetPath = resolve(__dirname, '..', '..', 'src', 'assets', 'lib', 'go');

		mkdirpSync(srcPath);

		writeFileSync(
			resolve(srcPath, 'main.go'),
			readFileSync(resolve(assetPath, 'main.go'), {
				encoding: 'utf-8',
			}).replace('libName', toCamelCase(libName)),
		);

		writeFileSync(
			resolve(srcPath, 'main_test.go'),
			readFileSync(resolve(assetPath, 'main_test.go'), {
				encoding: 'utf-8',
			}).replace('libName', toCamelCase(libName)),
		);
	}
}
