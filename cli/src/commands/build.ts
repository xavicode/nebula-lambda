import { Args, Command } from '@oclif/core';
import { blue, yellow } from 'colors/safe';

import { build } from '../utils/build';
import { getApps } from '../utils/config';

export default class Build extends Command {
	static description = 'Build services';

	static examples = ['<%= config.bin %> <%= command.id %>'];

	static flags = {};

	static args = {
		name: Args.string({ description: 'Application name' }),
	};

	public async run(): Promise<void> {
		const { args } = await this.parse(Build);
		const apps = getApps();
		if (!!apps.length && (args.name ? apps.includes(args.name) : true)) {
			this.log(blue('--- Build output'));

			if (args.name) {
				await build(args.name);
			} else {
				await build();
			}
		} else {
			this.log(yellow('Alert: No apps to run server'));
		}
	}
}
