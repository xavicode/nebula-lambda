import { resolve } from 'path';
import {
	copyFileSync,
	existsSync,
	lstatSync,
	mkdirpSync,
	readdirSync,
	readFileSync,
	readJSONSync,
	rmSync,
	writeFileSync,
	writeJSONSync,
} from 'fs-extra';
import { Args, Command } from '@oclif/core';
import { globSync } from 'glob';
import { execSync } from 'child_process';
import { red } from 'colors/safe';
import * as TurndownService from 'turndown';
import { OpenAPIObject } from '@nestjs/swagger';

import { rootPath } from '../utils/const';
import { getConfigApp } from '../utils/config';

export default class Docs extends Command {
	static description = 'Generate document assets';

	static examples = [
		'<%= config.bin %> <%= command.id %> dev',
		'<%= config.bin %> <%= command.id %> build',
		'<%= config.bin %> <%= command.id %> diagram',
		'<%= config.bin %> <%= command.id %> openapi',
	];

	static flags = {};

	static args = {
		command: Args.string({ description: 'Command arg', options: ['dev', 'build', 'diagram', 'openapi'] }),
	};

	public async run(): Promise<void> {
		const { args } = await this.parse(Docs);

		if (args.command === 'dev') {
			this.generateDiagram();
			this.generateOpenApi();
			await this.generateDocs();
			this.gitAddDocs();
			execSync('vitepress dev docs', { cwd: rootPath, stdio: 'inherit' });
		} else if (args.command === 'build') {
			this.generateDiagram();
			this.generateOpenApi();
			await this.generateDocs();
			this.gitAddDocs();
			execSync('vitepress build docs', { cwd: rootPath, stdio: 'inherit' });
		} else if (args.command === 'diagram') {
			this.generateDiagram();
			this.gitAddDocs();
		} else if (args.command === 'openapi') {
			this.generateOpenApi();
			this.gitAddDocs();
		} else {
			this.log(red('Error: Just enter an command arg'));
		}
	}

	private generateDiagram() {
		execSync('poetry run python docs/diagram.py', { cwd: rootPath, stdio: 'inherit' });
	}

	private generateOpenApi() {
		const openapi: OpenAPIObject = {
			openapi: '3.0.0',
			...readJSONSync(resolve(rootPath, 'config', 'openapi.base.json')),
			paths: {},
		};
		if (existsSync(resolve(rootPath, 'apps'))) {
			const apps = readdirSync(resolve(rootPath, 'apps'));
			if (apps.length) {
				if (!existsSync(resolve(rootPath, 'docs', 'public'))) {
					mkdirpSync(resolve(rootPath, 'docs', 'public'));
				}
				if (!existsSync(resolve(rootPath, 'docs', 'public', '_openapi'))) {
					mkdirpSync(resolve(rootPath, 'docs', 'public', '_openapi'));
				}
				for (const appName of apps) {
					const appPath = resolve(rootPath, 'apps', appName);
					if (existsSync(appPath)) {
						const app = getConfigApp(appName);
						const appOpenApiPath = resolve(rootPath, 'docs', 'public', '_openapi', appName + '.json');
						if (app) {
							let is_api = false;
							for (const evnt in app.Resource.Properties.Events) {
								if (app.Resource.Properties.Events[evnt].Type === 'Api') {
									is_api = true;
									break;
								}
							}
							if (is_api) {
								if (app.Language === 'nest') {
									execSync(
										`ts-node -r tsconfig-paths/register -P apps/${appName}/tsconfig.app.json apps/${appName}/src/openapi.ts`,
										{
											cwd: rootPath,
											stdio: 'inherit',
										},
									);
								} else if (app.Language === 'ts') {
									copyFileSync(resolve(appPath, 'openapi.json'), appOpenApiPath);
								} else if (app.Language === 'py') {
									execSync(`poetry run python apps/${appName}/openapi.py`, {
										cwd: rootPath,
										stdio: 'inherit',
									});
								} else if (app.Language === 'go') {
									copyFileSync(resolve(appPath, 'openapi.json'), appOpenApiPath);
								}
							}
						}
						if (existsSync(appOpenApiPath)) {
							const appOpenApi: OpenAPIObject = readJSONSync(appOpenApiPath);
							openapi.paths = { ...openapi.paths, ...appOpenApi.paths };
						}
					}
				}
			}
		}
		writeJSONSync(resolve(rootPath, 'docs', 'public', 'openapi.json'), openapi);
		rmSync(resolve(rootPath, 'docs', 'public', '_openapi'), { recursive: true, force: true });
	}

	private gitAddDocs() {
		execSync(`git add ${resolve(rootPath, 'docs')}/*`, { cwd: rootPath });
	}

	private async generateDocs() {
		const createDirDocs = (doc_dir: string) => {
			const docsPath = resolve(rootPath, 'docs', doc_dir);
			rmSync(docsPath, { recursive: true, force: true });
			mkdirpSync(docsPath);
		};
		if (existsSync(resolve(rootPath, 'apps'))) {
			const apps = readdirSync(resolve(rootPath, 'apps'));
			if (apps.length) {
				for (const appName of apps) {
					if (existsSync(resolve(rootPath, 'apps', appName))) {
						const app = getConfigApp(appName);
						if (app) {
							createDirDocs('apps/' + appName);
							if (app.Language === 'ts') {
								this.generateDocsTs('apps', 'apps', appName);
							} else if (app.Language === 'py') {
								this.generateDocsPy('apps', 'apps', appName);
							} else if (app.Language === 'go') {
								this.generateDocsGo('apps', 'apps', appName);
							}
						}
					}
				}
			}
		}
		for (const lib of ['libs', 'modules', 'packages']) {
			if (existsSync(resolve(rootPath, 'libs'))) {
				const libs = readdirSync(resolve(rootPath, lib));
				if (libs.length) {
					for (const libName of libs) {
						if (lib === 'libs') {
							createDirDocs('libs/ts/' + libName);
							this.generateDocsTs(lib, 'libs/ts', libName);
						} else if (lib === 'modules') {
							createDirDocs('libs/py/' + libName);
							this.generateDocsPy(lib, 'libs/py', libName);
						} else {
							createDirDocs('libs/go/' + libName);
							this.generateDocsGo(lib, 'libs/go', libName);
						}
					}
				}
			}
		}
	}

	private generateDocsTs(dir: string, dir_doc: string, name: string) {
		execSync(
			`typedoc --plugin typedoc-plugin-markdown --name ${name} --out docs/${dir_doc}/${name} ${dir}/${name}/src/*`,
			{
				cwd: rootPath,
				stdio: 'inherit',
			},
		);
		const docsPath = resolve(rootPath, 'docs', dir_doc, name);
		rmSync(resolve(docsPath, 'README.md'));
		rmSync(resolve(docsPath, '.nojekyll'));
		for (const file of globSync(docsPath + '/**/*')) {
			if (lstatSync(file).isFile()) {
				let doc = readFileSync(file, {
					encoding: 'utf-8',
				});
				doc = doc.split(`[${name}](README.md) /`).join('');
				doc = doc.split(`[${name}](../README.md) /`).join('');
				doc = doc.split('[Modules](../modules.md)').join(`[${name}](../modules.md)`);
				writeFileSync(file, doc);
			}
		}
	}

	private generateDocsPy(dir: string, dir_doc: string, name: string) {
		execSync(`${rootPath}/.venv/bin/pycco ${dir}/${name}/*.py -d=docs/${dir_doc}/${name}`, {
			cwd: rootPath,
			stdio: 'inherit',
		});
		const turndownService = new TurndownService();
		turndownService.addRule('code', {
			filter: (node) => node.classList.contains('code'),
			replacement: (content) => '```python' + content + '```',
		});
		turndownService.addRule('slash-on-code', {
			filter: (node) =>
				node.classList.contains('o') || node.classList.contains('n') || node.classList.contains('nf'),
			replacement: (content) => content.split('\\').join(''),
		});
		const convetToMarkdown = (path: string) => {
			for (const file of readdirSync(path)) {
				if (lstatSync(resolve(path, file)).isFile()) {
					if (file.split('.html')[0] !== 'openapi') {
						writeFileSync(
							resolve(path, file.split('.html').join('.md')),
							turndownService.turndown(readFileSync(resolve(path, file), { encoding: 'utf-8' })),
						);
					}
					rmSync(resolve(path, file));
				} else {
					convetToMarkdown(resolve(path, file));
				}
			}
		};

		convetToMarkdown(resolve(rootPath, 'docs', dir_doc, name));
	}

	private generateDocsGo(dir: string, dir_doc: string, name: string) {
		execSync(`gomarkdoc --output docs/${dir_doc}/${name}/index.md ./${dir}/${name}/`, {
			cwd: rootPath,
			stdio: 'inherit',
		});
		const docsPath = resolve(rootPath, 'docs', dir_doc, name);
		let doc = readFileSync(resolve(docsPath, 'index.md'), {
			encoding: 'utf-8',
		});
		const lines = doc.split('\n');
		lines.splice(0, 2);
		doc = lines.join('\n').split('Generated by [gomarkdoc]')[0];
		writeFileSync(resolve(docsPath, 'index.md'), doc);
	}
}
