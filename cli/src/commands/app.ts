import { resolve } from 'path';
import {
	copyFileSync,
	existsSync,
	mkdirpSync,
	readFileSync,
	readJSONSync,
	writeFileSync,
	writeJSONSync,
} from 'fs-extra';
import { Args, Command, Flags } from '@oclif/core';
import { execSync } from 'child_process';
import { blue, red } from 'colors/safe';
import { getConfig, getConfigApp, setConfig, setConfigApp } from '../utils/config';
import { toCamelCase, toKebabCase, toPascalCase, toSnakeCase } from '../utils/strings';

import { rootPath } from '../utils/const';

export default class App extends Command {
	static description = 'Generate a new app for aws lambda';

	static examples = [
		'<%= config.bin %> <%= command.id %> <name> --nest [--api]',
		'<%= config.bin %> <%= command.id %> <name> --ts [--api]',
		'<%= config.bin %> <%= command.id %> <name> --py [--api]',
		'<%= config.bin %> <%= command.id %> <name> --go [--api]',
		'<%= config.bin %> <%= command.id %> <name> --py [--docker, -d]',
	];

	static flags = {
		nest: Flags.boolean({
			description: 'Nest.js (TypeScript) application',
		}),
		ts: Flags.boolean({
			description: 'TypeScript application language',
		}),
		py: Flags.boolean({
			description: 'Python application language',
		}),
		go: Flags.boolean({
			description: 'Go application language',
		}),
		api: Flags.boolean({
			description: 'Api application type',
		}),
		docker: Flags.boolean({
			char: 'd',
			description: 'Docker application',
		}),
	};

	static args = {
		name: Args.string({ description: 'Application name' }),
	};

	public async run(): Promise<void> {
		const { args, flags } = await this.parse(App);

		if (args.name) {
			if (
				(flags.nest && flags.ts && flags.py && flags.go) ||
				(flags.nest && flags.ts) ||
				(flags.nest && flags.py) ||
				(flags.nest && flags.go) ||
				(flags.ts && flags.py) ||
				(flags.ts && flags.go) ||
				(flags.py && flags.go)
			) {
				this.log(red('Error: Just enter an application language (runtime)'));
			} else {
				if (flags.nest || flags.ts || flags.py || flags.go) {
					if (!existsSync(resolve(rootPath, 'apps'))) {
						mkdirpSync(resolve(rootPath, 'apps'));
					}
					const appName = toKebabCase(args.name);
					if (
						existsSync(resolve(rootPath, 'apps', appName)) ||
						existsSync(resolve(rootPath, 'libs', appName))
					) {
						this.log(red(`Error: Application/Library "${appName}" exists in this project already.`));
					} else {
						if (flags.api) {
							if (flags.nest) {
								await this.apiNest(appName);
							} else if (flags.ts) {
								await this.apiTs(appName);
							} else if (flags.py) {
								await this.apiPy(appName);
							} else if (flags.go) {
								await this.apiGo(appName);
							}
						} else {
							if (flags.nest) {
								await this.standaloneNest(appName);
							} else if (flags.ts) {
								await this.standaloneTs(appName);
							} else if (flags.py) {
								await this.standalonePy(appName);
							} else if (flags.go) {
								await this.standaloneGo(appName);
							}
						}

						if (existsSync(resolve(rootPath, '.git')) && existsSync(resolve(rootPath, 'apps', appName))) {
							execSync(`git add ${resolve(rootPath, 'apps', appName)}/*`, { cwd: rootPath });
						}

						execSync('npm run format', { cwd: rootPath, stdio: 'inherit' });
					}
				} else {
					// prettier-ignore
					this.log(red('Error: Can\'t continue without an application language'));
				}
			}
		} else {
			// prettier-ignore
			this.log(red('Error: Can\'t continue without an application name'));
		}
	}

	private replaceAppName(appName: string, p: string) {
		const kebabAppName = appName;
		const camelAppName = toCamelCase(appName);
		const pascalAppName = toPascalCase(appName);
		const snakeAppName = toSnakeCase(appName);
		return readFileSync(p, {
			encoding: 'utf-8',
		})
			.split('app-name')
			.join(kebabAppName)
			.split('appName')
			.join(camelAppName)
			.split('AppName')
			.join(pascalAppName)
			.split('app_name')
			.join(snakeAppName);
	}

	private paths(appName: string, type: 'api' | 'docker' | 'standalone', lang: 'nest' | 'ts' | 'py' | 'go') {
		const srcPath = resolve(rootPath, 'apps', appName);
		const assetPath = resolve(__dirname, '..', '..', 'src', 'assets', 'app', type, lang);

		mkdirpSync(srcPath);

		if (lang === 'nest' || lang === 'ts') {
			mkdirpSync(resolve(srcPath, 'src'));
		}

		return { srcPath, assetPath };
	}

	private async apiNest(appName: string) {
		this.log(blue('--- Nest.js (Typescript) app'));

		const { srcPath, assetPath } = this.paths(appName, 'api', 'nest');

		writeFileSync(
			resolve(srcPath, 'tsconfig.app.json'),
			this.replaceAppName(appName, resolve(assetPath, 'tsconfig.app.json')),
		);

		writeFileSync(
			resolve(srcPath, 'src', 'main.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'main.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', 'openapi.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'openapi.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.controller.spec.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.controller.spec.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.controller.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.controller.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.service.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.service.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.module.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.module.ts')),
		);

		// nest-cli.json
		const nest = readJSONSync(resolve(rootPath, 'config', 'nest-cli.json'));

		nest['projects'][appName] = {
			type: 'application',
			root: 'apps/' + appName,
			entryFile: 'main',
			sourceRoot: 'apps/' + appName + '/src',
			compilerOptions: {
				tsConfigPath: 'apps/' + appName + '/tsconfig.app.json',
			},
		};

		writeJSONSync(resolve(rootPath, 'config', 'nest-cli.json'), nest);

		await this.configApp(appName);
	}

	private async apiTs(appName: string) {
		this.log(blue('--- Typescript app'));

		const { srcPath, assetPath } = this.paths(appName, 'api', 'ts');

		writeFileSync(
			resolve(srcPath, 'tsconfig.app.json'),
			this.replaceAppName(appName, resolve(assetPath, 'tsconfig.app.json')),
		);

		writeFileSync(
			resolve(srcPath, 'src', 'main.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'main.ts')),
		);

		writeFileSync(
			resolve(srcPath, 'openapi.json'),
			this.replaceAppName(appName, resolve(assetPath, 'openapi.json')),
		);

		await this.configApp(appName);
	}

	private async apiPy(appName: string) {
		this.log(blue('--- Python app'));

		const { srcPath, assetPath } = this.paths(appName, 'api', 'py');

		copyFileSync(resolve(assetPath, 'controller.py'), resolve(srcPath, 'controller.py'));

		writeFileSync(
			resolve(srcPath, 'controller_test.py'),
			this.replaceAppName(appName, resolve(assetPath, 'controller_test.py')),
		);

		writeFileSync(resolve(srcPath, 'main.py'), this.replaceAppName(appName, resolve(assetPath, 'main.py')));

		writeFileSync(resolve(srcPath, 'openapi.py'), this.replaceAppName(appName, resolve(assetPath, 'openapi.py')));

		await this.configApp(appName);
	}

	private async apiGo(appName: string) {
		this.log(blue('--- Go app'));

		const { srcPath, assetPath } = this.paths(appName, 'api', 'go');

		copyFileSync(resolve(assetPath, 'controller.go'), resolve(srcPath, 'controller.go'));
		copyFileSync(resolve(assetPath, 'controller_test.go'), resolve(srcPath, 'controller_test.go'));

		writeFileSync(resolve(srcPath, 'main.go'), this.replaceAppName(appName, resolve(assetPath, 'main.go')));

		writeFileSync(
			resolve(srcPath, 'openapi.json'),
			this.replaceAppName(appName, resolve(assetPath, 'openapi.json')),
		);

		await this.configApp(appName);
	}

	private async standaloneNest(appName: string) {
		this.log(blue('--- Nest.js (Typescript) app'));

		const { srcPath, assetPath } = this.paths(appName, 'standalone', 'nest');

		writeFileSync(
			resolve(srcPath, 'tsconfig.app.json'),
			this.replaceAppName(appName, resolve(assetPath, 'tsconfig.app.json')),
		);

		writeFileSync(
			resolve(srcPath, 'src', 'main.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'main.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.service.spec.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.service.spec.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.service.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.service.ts')),
		);
		writeFileSync(
			resolve(srcPath, 'src', appName + '.module.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'app-name.module.ts')),
		);

		// nest-cli.json
		const nest = readJSONSync(resolve(rootPath, 'config', 'nest-cli.json'));

		nest['projects'][appName] = {
			type: 'application',
			root: 'apps/' + appName,
			entryFile: 'main',
			sourceRoot: 'apps/' + appName + '/src',
			compilerOptions: {
				tsConfigPath: 'apps/' + appName + '/tsconfig.app.json',
			},
		};

		writeJSONSync(resolve(rootPath, 'config', 'nest-cli.json'), nest);

		await this.configApp(appName);
	}

	private async standaloneTs(appName: string) {
		this.log(blue('--- Typescript app'));

		const { srcPath, assetPath } = this.paths(appName, 'standalone', 'ts');

		writeFileSync(
			resolve(srcPath, 'tsconfig.app.json'),
			this.replaceAppName(appName, resolve(assetPath, 'tsconfig.app.json')),
		);

		writeFileSync(
			resolve(srcPath, 'src', 'main.ts'),
			this.replaceAppName(appName, resolve(assetPath, 'src', 'main.ts')),
		);

		await this.configApp(appName);
	}

	private async standalonePy(appName: string) {
		this.log(blue('--- Python app'));

		const { srcPath, assetPath } = this.paths(appName, 'standalone', 'py');

		writeFileSync(
			resolve(srcPath, 'main_test.py'),
			this.replaceAppName(appName, resolve(assetPath, 'main_test.py')),
		);
		writeFileSync(resolve(srcPath, 'main.py'), this.replaceAppName(appName, resolve(assetPath, 'main.py')));

		await this.configApp(appName);
	}

	private async standaloneGo(appName: string) {
		this.log(blue('--- Go app'));

		const { srcPath, assetPath } = this.paths(appName, 'standalone', 'go');

		writeFileSync(
			resolve(srcPath, 'main_test.go'),
			this.replaceAppName(appName, resolve(assetPath, 'main_test.go')),
		);
		writeFileSync(resolve(srcPath, 'main.go'), this.replaceAppName(appName, resolve(assetPath, 'main.go')));

		await this.configApp(appName);
	}

	private async configApp(appName: string) {
		this.log(blue('--- Config app'));

		const { flags } = await this.parse(App);

		const app = getConfigApp(appName, flags.nest ? 'nest' : flags.ts ? 'ts' : flags.py ? 'py' : 'go');

		if (app) {
			if (app.Language === 'nest' || app.Language === 'ts') {
				app.Resource.Properties.Handler = 'main.handler';
				app.Resource.Properties.Runtime = 'nodejs18.x';
			} else if (app.Language === 'py') {
				app.Resource.Properties.Handler = 'main.handler';
				app.Resource.Properties.Runtime = 'python3.9';
			} else if (app.Language === 'go') {
				app.Resource.Properties.Handler = 'main';
				app.Resource.Properties.Runtime = 'go1.x';
			}

			if (flags.docker) {
				app.PackageType = 'docker';
				app.ImageRepository = '';
				app.ImageTag = 'latest';
				const { srcPath, assetPath } = this.paths(appName, 'docker', app.Language);

				copyFileSync(resolve(assetPath, 'Dockerfile'), resolve(srcPath, 'Dockerfile'));
			}

			if (flags.api) {
				app.Resource.Properties.Events[toPascalCase(appName)] = {
					Type: 'Api',
					Properties: {
						Path: '/' + appName,
						Method: 'get',
					},
				};
			}

			setConfigApp(app);

			const config = getConfig();

			if (app.Language === 'nest') {
				config.runtimes.nest = true;
			} else if (app.Language === 'ts') {
				config.runtimes.ts = true;
			} else if (app.Language === 'py') {
				config.runtimes.py = true;
			} else if (app.Language === 'go') {
				config.runtimes.go = true;
			}

			setConfig(config);
		}
	}
}
