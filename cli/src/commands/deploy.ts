import { Command, Flags } from '@oclif/core';
import { getApps, getConfig } from '../utils/config';
import { blue, yellow } from 'colors/safe';
import { build } from '../utils/build';
import { execSync } from 'child_process';
import { rootPath } from '../utils/const';

export default class Deploy extends Command {
	static description = 'Deploy app';

	static examples = ['<%= config.bin %> <%= command.id %>'];

	static flags = {
		destroy: Flags.boolean({
			char: 'd',
			description: ' Destroy the stack(s) deployed',
		}),
		force: Flags.boolean({
			char: 'f',
			description: 'Force deploy or destroy',
		}),
	};

	static args = {};

	public async run(): Promise<void> {
		const { flags } = await this.parse(Deploy);

		if (flags.destroy) {
			const config = getConfig();
			let cmd: string;
			if (config.profileEnv) {
				cmd = `cdk destroy --profile ${config.profileEnv}`;
			} else {
				cmd = 'cdk destroy';
			}
			if (flags.force) {
				cmd = cmd + ' --force';
			}
			execSync(cmd, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		} else {
			const apps = getApps();
			if (!!apps.length) {
				this.log(blue('--- Build output'));
				await build();

				const config = getConfig();

				this.log(blue('--- Deploy output'));
				let cmd: string;
				if (config.profileEnv) {
					cmd = `cdk bootstrap --profile ${config.profileEnv} && cdk deploy --profile ${config.profileEnv}`;
				} else {
					cmd = 'cdk bootstrap && cdk deploy';
				}
				if (flags.force) {
					cmd = cmd + ' --force';
				}
				execSync(cmd, {
					cwd: rootPath,
					stdio: 'inherit',
				});
			} else {
				this.log(yellow('Alert: No apps to deploy'));
			}
		}
	}
}
