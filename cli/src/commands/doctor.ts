import { Command } from '@oclif/core';
import { gte } from 'semver';
import { blue, green, red, yellow } from 'colors/safe';
import { existsSync } from 'fs-extra';
import { resolve } from 'path';

import { rootPath } from '../utils/const';
import { checkBin, getVersion } from '../utils/misc';
import { getConfig } from '../utils/config';

export default class Doctor extends Command {
	static description = 'Check if your system is ready for nebula-lambda cli';

	static examples = ['<%= config.bin %> <%= command.id %>'];

	static flags = {};

	static args = {};

	public async run(): Promise<void> {
		this.log(green('nebula-lambda doctor') + ' --- Check if your system is ready for nebula-lambda cli');
		this.log(blue('--- Environment diagnose:'));
		const config = getConfig();
		if (checkBin('node')) {
			const version_string = getVersion('node');
			if (gte(version_string, '18.0.0')) {
				this.log(`${green('[✓]')} Node.js ${version_string}`);
			} else {
				this.log(`${red('[✖]')} Node.js ${version_string} is less that 18.0.0`);
			}
		}
		if (checkBin('npm')) {
			const version_string = getVersion('npm');
			if (gte(version_string, '9.0.0')) {
				this.log(`${green('[✓]')} npm ${version_string}`);
			} else {
				this.log(`${red('[✖]')} npm ${version_string} is less that 9.0.0`);
			}
		}
		if (checkBin('pnpm')) {
			const version_string = getVersion('pnpm');
			if (gte(version_string, '8.0.0')) {
				this.log(`${green('[✓]')} pnpm ${version_string}`);
			} else {
				this.log(`${red('[✖]')} pnpm ${version_string} is less that 8.0.0`);
			}
		} else {
			this.log(`${red('[✖]')} pnpm is not available on your system`);
			this.log('	Install pnpm from https://pnpm.io/installation');
			this.log('	or install with `npm i -g pnpm`');
		}
		if (checkBin('cdk')) {
			const version_string = getVersion('cdk').split(' (build ')[0];
			if (gte(version_string, '2.80.0')) {
				this.log(`${green('[✓]')} AWS CDK CLI ${version_string}`);
			} else {
				this.log(`${red('[✖]')} AWS CDK CLI ${version_string} is less that 2.80.0`);
			}
		} else {
			this.log(`${yellow('[!]')} AWS CDK CLI is not available on your system, optional`);
			this.log(
				'	Install AWS CDK CLI from https://docs.aws.amazon.com/cdk/v2/guide/getting_started.html#getting_started_install',
			);
		}
		if (checkBin('dot')) {
			this.log(`${green('[✓]')} Graphviz`);
		} else {
			this.log(`${yellow('[!]')} Graphviz is not available on your system, required for documentation`);
			this.log('	Install Graphviz from https://graphviz.org/download/');
		}
		if (config.runtimes.py) {
			if (checkBin('python3.9') || checkBin('python3.10')) {
				const version_string = getVersion(checkBin('python3.9') ? 'python3.9' : 'python3.10').split(
					'Python ',
				)[1];
				if (gte(version_string, '3.9.0')) {
					this.log(`${green('[✓]')} Python ${version_string}`);
				} else {
					this.log(`${red('[✖]')} Python ${version_string} is less that 3.9.0`);
					this.log('	Install Python from https://www.python.org/downloads/');
				}
			} else {
				this.log(`${red('[✖]')} Python is not available on your system`);
				this.log('	Install Poetry from https://python-poetry.org/docs/');
			}
			if (checkBin('poetry')) {
				const version_string = getVersion('poetry').split('Poetry (version ')[1].split(')')[0];
				if (gte(version_string, '1.4.0')) {
					this.log(`${green('[✓]')} Poetry ${version_string}`);
				} else {
					this.log(`${red('[✖]')} Poetry ${version_string} is less that 1.4.0`);
				}
			} else {
				this.log(`${red('[✖]')} Poetry is not available on your system`);
				this.log('	Install Poetry from https://python-poetry.org/docs/');
			}
			if (existsSync(resolve(rootPath, '.venv'))) {
				this.log(`${green('[✓]')} Python virtual environment`);
			} else {
				this.log(`${red('[✖]')} Python virtual environment`);
				this.log('	Init with `poetry install`');
			}
		}
		if (config.runtimes.go) {
			if (checkBin('go')) {
				const version_string = getVersion('go', 'version').split('go version go')[1].split(' ')[0];
				if (gte(version_string, '1.20.0')) {
					this.log(`${green('[✓]')} Go ${version_string}`);
				} else {
					this.log(`${red('[✖]')} Go ${version_string} is less that 1.20.0`);
				}
			} else {
				this.log(`${red('[✖]')} Go is not available on your system`);
				this.log('	Install Go from https://go.dev/dl/');
			}
			if (existsSync(resolve(rootPath, 'vendor'))) {
				this.log(`${green('[✓]')} Go modules vendor`);
			} else {
				this.log(`${red('[✖]')} Go modules vendor`);
				this.log('	Prepare go modules vendor with `go mod`, example `go mod vendor`');
			}
			if (checkBin('golangci-lint')) {
				const version_string = getVersion('golangci-lint').split('golangci-lint has version ')[1].split(' ')[0];
				if (gte(version_string, '1.50.0')) {
					this.log(`${green('[✓]')} golangci-lint ${version_string}`);
				} else {
					this.log(`${red('[✖]')} golangci-lint ${version_string} is less that 1.50.0`);
				}
			} else {
				this.log(`${red('[✖]')} golangci-lint is not available on your system`);
				this.log('	Install golangci-lint from https://golangci-lint.run/usage/install/');
				this.log('	or install with `go get github.com/golangci/golangci-lint/cmd/golangci-lint`');
			}
			if (checkBin('gofmt')) {
				this.log(`${green('[✓]')} gofmt`);
			} else {
				this.log(`${red('[✖]')} gofmt is not available on your system`);
				this.log('	Install gofmt with `go get golang.org/x/tools/cmd/gofmt`');
			}
			if (checkBin('gomarkdoc')) {
				const version_string = getVersion('gomarkdoc');
				if (gte(version_string, '0.4.1')) {
					this.log(`${green('[✓]')} gomarkdoc ${version_string}`);
				} else {
					this.log(`${red('[✖]')} gomarkdoc ${version_string} is less that 0.4.1`);
				}
			} else {
				this.log(`${red('[✖]')} gomarkdoc is not available on your system`);
				this.log('	Install gomarkdoc from https://github.com/princjef/gomarkdoc');
				this.log('	or install with `go get github.com/princjef/gomarkdoc/cmd/gomarkdoc@latest`');
			}
		}
	}
}
