import { Command, Flags } from '@oclif/core';
import { blue, red } from 'colors/safe';
import { execSync } from 'child_process';
import { resolve } from 'path';
import { existsSync } from 'fs-extra';

import { rootPath } from '../utils/const';

export default class Test extends Command {
	static description = 'Test apps';

	static examples = [
		'<%= config.bin %> <%= command.id %> --unit --ts',
		'<%= config.bin %> <%= command.id %> --unit --py',
		'<%= config.bin %> <%= command.id %> --unit --go',
		'<%= config.bin %> <%= command.id %> --e2e',
	];

	static flags = {
		ts: Flags.boolean({
			description: 'Testing TypeScript code',
		}),
		py: Flags.boolean({
			description: 'Testing Python code',
		}),
		go: Flags.boolean({
			description: 'Testing Go code',
		}),
		apps: Flags.boolean({
			description: 'Testing apps',
		}),
		libs: Flags.boolean({
			description: 'Testing libs',
		}),
		unit: Flags.boolean({
			description: 'Unit testing',
		}),
		e2e: Flags.boolean({
			description: 'E2E testing',
		}),
	};

	static args = {};

	public async run(): Promise<void> {
		const { flags } = await this.parse(Test);

		if (flags.unit && flags.e2e) {
			this.log(red('Error: Just enter an testing type (unit | e2e)'));
		} else if (flags.unit || flags.e2e) {
			if (flags.unit) {
				if (flags.ts || flags.py || flags.go) {
					if (flags.ts) {
						if (flags.apps || flags.libs) {
							if (flags.apps) {
								await this.testAppsTs();
							}
							if (flags.libs) {
								await this.testLibsTs();
							}
						} else {
							await this.testAppsTs();
							await this.testLibsTs();
						}
					}
					if (flags.py) {
						if (flags.apps || flags.libs) {
							if (flags.apps) {
								await this.testAppsPy();
							}
							if (flags.libs) {
								await this.testLibsPy();
							}
						} else {
							await this.testAppsPy();
							await this.testLibsPy();
						}
					}
					if (flags.go) {
						if (flags.apps || flags.libs) {
							if (flags.apps) {
								await this.testAppsGo();
							}
							if (flags.libs) {
								await this.testLibsGo();
							}
						} else {
							await this.testAppsGo();
							await this.testLibsGo();
						}
					}
				} else {
					if (flags.apps || flags.libs) {
						if (flags.apps) {
							await this.testAppsTs();
							await this.testAppsPy();
							await this.testAppsGo();
						}
						if (flags.libs) {
							await this.testLibsTs();
							await this.testLibsPy();
							await this.testLibsGo();
						}
					} else {
						await this.testAppsTs();
						await this.testAppsPy();
						await this.testAppsGo();
						await this.testLibsTs();
						await this.testLibsPy();
						await this.testLibsGo();
					}
				}
			} else {
				await this.testE2E();
			}
		} else {
			this.log(red('Error: Enter an testing type (unit | e2e)'));
		}
	}

	private async testE2E() {
		this.log(blue('--- E2E testing'));
		const jestConfig = resolve(__dirname, '..', '..', 'src', 'assets', 'jest', 'e2e.config.ts');
		execSync(`jest --config ${jestConfig}`, {
			cwd: rootPath,
			stdio: 'inherit',
		});
	}

	private async testAppsTs() {
		this.log(blue('--- Typescript apps testing'));
		if (existsSync(resolve(rootPath, 'apps'))) {
			const jestConfig = resolve(__dirname, '..', '..', 'src', 'assets', 'jest', 'apps.config.ts');
			execSync(`jest --config ${jestConfig}`, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}

	private async testLibsTs() {
		this.log(blue('--- Typescript libs testing'));
		if (existsSync(resolve(rootPath, 'libs'))) {
			const jestConfig = resolve(__dirname, '..', '..', 'src', 'assets', 'jest', 'libs.config.ts');
			execSync(`jest --config ${jestConfig}`, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}

	private async testAppsPy() {
		this.log(blue('--- Python apps testing'));
		if (existsSync(resolve(rootPath, 'apps'))) {
			execSync(`${resolve(rootPath, '.venv', 'bin', 'pytest')} apps/`, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}

	private async testLibsPy() {
		this.log(blue('--- Python libs testing'));
		if (existsSync(resolve(rootPath, 'modules'))) {
			execSync(`${resolve(rootPath, '.venv', 'bin', 'pytest')} modules/`, {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}

	private async testAppsGo() {
		this.log(blue('--- Go apps testing'));
		if (existsSync(resolve(rootPath, 'apps'))) {
			execSync('go clean -modcache -cache -i && go test -v ./apps/...', {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}

	private async testLibsGo() {
		this.log(blue('--- Go libs testing'));
		if (existsSync(resolve(rootPath, 'packages'))) {
			execSync('go clean -modcache -cache -i && go test -v ./packages/...', {
				cwd: rootPath,
				stdio: 'inherit',
			});
		}
	}
}
