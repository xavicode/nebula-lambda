# Nebula Lambda CLI

This template has some CLI commands that make development, testing and deployment easier. Run `nebula-lambda --help` to read more details.

## Install project

``` shell
npm run configure
```

Run the command at the project's root to install the necessary dependencies and kickstart the project. If you make any changes to the CLI, execute this command to apply the updates.

## Environment diagnose

``` shell
nebula-lambda doctor
```

Check if your system is ready for nebula-lambda cli.

## New function (app)

``` shell
nebula-lambda app appName --nest [--api] [--docker]
nebula-lambda app appName --ts [--api]  [--docker]
nebula-lambda app appName --py [--api]  [--docker]
nebula-lambda app appName --go [--api]  [--docker]
```

You can create new Lambda functions in the following programming languages: TypeScript, Python, and Go. Each Lambda function, in its respective language, comes pre-written with a set of pre-selected tools.

**Nest.js:**
- **any** event
	- Nest.js

**TypeScript:**
- **any** event
	- Middy.js

**Python:**
- **api** event
	- fastapi
	- mangum
- **any** event
	- Plain Python
    - Powertools for AWS Lambda

**Go:**
- **api** event
	- github.com/labstack/echo/v4
	- github.com/awslabs/aws-lambda-go-api-proxy
- **any** event
	- github.com/aws/aws-lambda-go/events
    - github.com/mefellows/vesper

## New library

``` shell
nebula-lambda lib libName --nest
nebula-lambda lib libName --ts
nebula-lambda lib libName --py
nebula-lambda lib libName --go
```

You can create new libraries for your Lambda functions using the following programming languages: TypeScript, Python, and Go.. Each Lambda function, in its respective language, comes pre-written with a set of pre-selected tools.

**Nest.js:**
- Nest.js

**TypeScript:**
- Middy.js

**Python:**
- Plain Python
- Powertools for AWS Lambda

**Go:**
- Plain Go

## Build

``` shell
nebula-lambda deploy
```

Build your Lambda functions.

## Deploy

``` shell
nebula-lambda deploy [--force] [--destroy]
```

Build and deploy your Lambda functions directly on AWS.

## AWS CDK

This project uses AWS CDK to manage resources and deployments in AWS.
