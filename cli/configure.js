// eslint-disable-next-line @typescript-eslint/no-var-requires
const { resolve } = require('path');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { execSync } = require('child_process');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { rmSync } = require('fs');

const rootPath = process.cwd();

function checkBin(bin) {
	let res;
	try {
		res = execSync((process.platform === 'win32' ? 'where' : 'which ') + bin, { encoding: 'utf-8' });
	} catch (e) {
		res = e.toString();
	}
	return !(res.search('Error: Command failed: which') >= 0);
}

rmSync(resolve(rootPath, 'node_modules'), { force: true, recursive: true });
rmSync(resolve(rootPath, 'dist'), { force: true, recursive: true });
rmSync(resolve(rootPath, '.pytest_cache'), { force: true, recursive: true });
rmSync(resolve(rootPath, 'cli', 'node_modules'), { force: true, recursive: true });
rmSync(resolve(rootPath, 'cli', 'dist'), { force: true, recursive: true });

if (checkBin('pnpm')) {
	execSync('pnpm install', { cwd: resolve(rootPath, 'cli'), stdio: 'inherit' });
	execSync('pnpm install', { cwd: rootPath, stdio: 'inherit' });
	execSync('nebula-lambda doctor', { cwd: rootPath, stdio: 'inherit' });
} else {
	console.log('\u001b[31m pnpm is not available on your system');
	console.log('	Install pnpm from https://pnpm.io/installation');
	console.log('	or install with `npm i -g pnpm`');
}
