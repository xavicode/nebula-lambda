const webpack = require('webpack');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = (env) => {
	return {
		entry: `./apps/${env.AppName}/src/main.ts`,
		devtool: false,
		target: 'node',
		output: { filename: `./apps/${env.AppName}/main.js`, libraryTarget: 'commonjs2' },
		ignoreWarnings: [/^(?!CriticalDependenciesWarning$)/],
		externalsPresets: { node: true },
		module: {
			rules: [
				{
					test: /.ts/,
					loader: 'ts-loader',
					exclude: /node_modules/,
				},
				{
					test: /\.js$/,
					loader: 'babel-loader',
				},
			],
		},
		resolve: {
			extensions: ['.ts', '.js'],
			plugins: [
				new TsconfigPathsPlugin({
					configFile: `./apps/${env.AppName}/tsconfig.app.json`,
				}),
			],
		},
		mode: 'none',
		optimization: {
			nodeEnv: false,
		},
		node: {
			__filename: false,
			__dirname: false,
		},
		plugins: [
			new webpack.IgnorePlugin({
				checkResource(resource) {
					const lazyImports = ['cache-manager', 'class-validator', 'class-transformer'];
					if (!lazyImports.includes(resource)) {
						return false;
					}
					try {
						require.resolve(resource, {
							paths: [process.cwd()],
						});
					} catch (err) {
						return true;
					}
					return false;
				},
			}),
			new ForkTsCheckerWebpackPlugin({
				typescript: {
					configFile: `./apps/${env.AppName}/tsconfig.app.json`,
				},
			}),
		],
	};
};
