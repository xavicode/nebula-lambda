import * as lambda from 'aws-cdk-lib/aws-lambda';
import { Construct } from 'constructs';
import { getConfig, getAppsConfig, Event } from 'nebula-lambda-cli/dist/utils/config';

/**
 * IResources - An interface for objects that return the information of the generated resources.
 */
export interface IResources {
	[id: string]: { function: lambda.Function; events: { [p: string]: Event } };
}

/**
 * getResources - Generates the resources for the Lambda functions located in the /apps directory..
 *
 * @param scope - An instance of the Construct class.
 * @returns IResources
 */
export function getResources(scope: Construct) {
	const resources: IResources = {};
	const config = getConfig();
	for (const app of getAppsConfig()) {
		const handler_id = config.getResourceId(app.Name);
		let runtime = lambda.Runtime.NODEJS_18_X;
		let handler: lambda.Function;
		if (app.Language === 'py') {
			runtime = lambda.Runtime.PYTHON_3_10;
		} else if (app.Language === 'go') {
			runtime = lambda.Runtime.GO_1_X;
		}
		const functionName = handler_id;
		const architecture: lambda.Architecture =
			app.Resource.Properties.Architectures && app.Resource.Properties.Architectures.length
				? app.Resource.Properties.Architectures[0] === 'x86_64'
					? lambda.Architecture.X86_64
					: lambda.Architecture.ARM_64
				: lambda.Architecture.X86_64;
		const environment: {
			[key: string]: string;
		} = {
			ENV: config.stage,
			...app.Resource.Properties.Environment.Variables,
		};
		if (['nest', 'ts', 'go'].includes(app.Language)) {
			handler = new lambda.Function(scope, handler_id, {
				runtime,
				functionName,
				architecture,
				code: lambda.Code.fromAsset('dist/apps/' + app.Name),
				handler: app.Resource.Properties.Handler,
				environment,
			});
		} else if (app.Language === 'py') {
			handler = new lambda.Function(scope, handler_id, {
				runtime,
				functionName,
				architecture,
				code: lambda.Code.fromAsset('dist/apps/' + app.Name, {
					bundling: {
						image: lambda.Runtime.PYTHON_3_9.bundlingImage,
						command: [
							'bash',
							'-c',
							'pip install -r requirements.txt -t /asset-output && cp -au . /asset-output',
						],
					},
				}),
				handler: app.Resource.Properties.Handler,
				environment,
			});
		}

		if (!!handler) {
			resources[handler_id] = { function: handler, events: app.Resource.Properties.Events };
		}
	}

	return resources;
}
