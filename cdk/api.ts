import * as apigateway from 'aws-cdk-lib/aws-apigateway';
import { Construct } from 'constructs';
import { getConfig } from 'nebula-lambda-cli/dist/utils/config';
import { IResources } from './resource';

/**
 * ElementPathResource - A resource tree for organizing the routes of the REST API.
 */
interface ElementPathResource {
	resource: apigateway.IResource;
	children: { [p: string]: ElementPathResource };
}

/**
 * getApi - Generates the resources for the Lambda functions located in the /apps directory..
 *
 * @param scope - An instance of the Construct class.
 * @param resources - Resources for the Lambda functions.
 * @returns apigateway.RestApi | undefined
 */
export function getApi(scope: Construct, resources: IResources): apigateway.RestApi | undefined {
	let api: apigateway.RestApi;
	const config = getConfig();
	const treePathsResourcesApi: { [p: string]: ElementPathResource } = {};
	for (const handler_id in resources) {
		const resource = resources[handler_id];
		const isApi = !!Object.values(resource.events).filter((evt) => evt.Type === 'Api').length;

		if (isApi) {
			if (!api) {
				api = new apigateway.RestApi(scope, config.getResourceId('apigateway'), {
					restApiName: config.getResourceName('apigateway'),
					deployOptions: {
						stageName: config.stage,
					},
					defaultCorsPreflightOptions: {
						allowHeaders: ['Content-Type', 'X-Amz-Date', 'Authorization', 'X-Api-Key'],
						allowMethods: ['OPTIONS', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
						allowCredentials: true,
						allowOrigins: ['*'],
					},
				});

				treePathsResourcesApi[''] = {
					resource: api.root,
					children: {},
				};
			}

			const handlerIntegration = new apigateway.LambdaIntegration(resource.function, {
				requestTemplates: { 'application/json': '{ "statusCode": "200" }' },
				proxy: true,
			});

			for (const key of Object.keys(resource.events)) {
				const evt = resource.events[key];

				if (evt.Type === 'Api') {
					const paths = evt.Properties['Path'].split('/');

					let resourcePath: apigateway.IResource = treePathsResourcesApi[''].resource;
					let treeSearch: { [p: string]: ElementPathResource } = treePathsResourcesApi;

					for (const p of paths) {
						if (p) {
							if (treeSearch[p]) {
								resourcePath = treeSearch[p].resource;
								treeSearch = treeSearch[p].children;
							} else {
								treeSearch[p] = {
									resource: resourcePath.addResource(p),
									children: {},
								};
								resourcePath = treeSearch[p].resource;
							}
						} else {
							resourcePath = treePathsResourcesApi[''].resource;
						}
					}
					resourcePath.addMethod(evt.Properties['Method'].toUpperCase(), handlerIntegration);
				}
			}
		}
	}
	return api;
}
