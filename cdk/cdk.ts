#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from 'aws-cdk-lib';
import { NebulaStack } from './nebula';
import { getConfig } from 'nebula-lambda-cli/dist/utils/config';

const app = new cdk.App();

const config = getConfig();

new NebulaStack(app, config.nameApp, { env: config.environment });
