import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { getResources } from './resource';
import { getApi } from './api';

export class NebulaStack extends cdk.Stack {
	constructor(scope: Construct, id: string, props?: cdk.StackProps) {
		super(scope, id, props);

		const resources = getResources(this);
		getApi(this, resources);
	}
}
