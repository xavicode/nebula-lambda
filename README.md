# Nebula Lambda

This template has tools for developing, building, and deploying AWS Lambda applications written in Go, Python, or
TypeScript.

## Install project

``` shell
npm run configure
```

Run the command at the project's root to install the necessary dependencies and kickstart the project.

## [Docs](./docs/index.md)

Documentation of this project.

## [Cli](./cli/readme.md)

This template comes with development tools and dependency packages useful for working with the functions.




