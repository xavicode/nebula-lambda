from diagrams import Diagram
from diagrams.aws.compute import Lambda
from diagrams.aws.network import APIGateway
from diagrams.generic.device import Mobile

with Diagram("Nebula Lambda", show=False, filename="docs/public/diagram"):
    client = Mobile("Client")
    api = APIGateway("Api")
    function = Lambda("Function")

    client >> api
    api >> function
    function >> api
    api >> client
