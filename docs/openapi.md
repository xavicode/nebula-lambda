<div ref="swagger"></div>

<script setup>
import { onMounted, ref } from 'vue';
import { withBase } from 'vitepress';
import { SwaggerUIBundle } from 'swagger-ui-dist';
import 'swagger-ui-dist/swagger-ui.css';

const swagger = ref(null);

onMounted(() => {
	SwaggerUIBundle({
    	url: withBase('/openapi.json'),
    	domNode: swagger.value
	});
})
</script>

<style>
.dark .swagger-ui {
    filter: invert(88%) hue-rotate(180deg);
}

.dark .swagger-ui .highlight-code {
    filter: invert(100%) hue-rotate(180deg);
}
</style>
