import { DefaultTheme, defineConfig } from 'vitepress';
import { SearchPlugin } from 'vitepress-plugin-search';
import { existsSync, lstatSync, readdirSync } from 'fs';
import { resolve } from 'path';

const rootPath = process.cwd();

const getDirNav = (url: string, path: string) => {
	const dir_nav: DefaultTheme.SidebarItem[] = [];
	for (const file of readdirSync(path)) {
		if (lstatSync(resolve(path, file)).isFile()) {
			if (file.split('.md')[0] !== 'openapi') {
				dir_nav.push({
					text: file.split('.md')[0],
					link: url + '/' + file,
				});
			}
		} else {
			dir_nav.push({
				text: file,
				collapsed: true,
				items: getDirNav(url + '/' + file, resolve(path, file)),
			});
		}
	}
	return dir_nav;
};

const getAppSidebarItems = () => {
	const apps_nav: DefaultTheme.SidebarItem[] = [];
	if (existsSync(resolve(rootPath, 'docs', 'apps'))) {
		const apps = readdirSync(resolve(rootPath, 'docs', 'apps'));
		if (apps.length) {
			for (const appName of apps) {
				if (!appName.endsWith('.md') && appName !== '.DS_Store') {
					const pathApp = resolve(rootPath, 'docs', 'apps', appName);
					apps_nav.push({
						text: appName,
						collapsed: true,
						items: getDirNav('apps/' + appName, pathApp),
					});
				}
			}
		}
	}

	return apps_nav;
};

const getLibSidebarItems = (lang: 'ts' | 'py' | 'go') => {
	const libs_nav: DefaultTheme.SidebarItem[] = [];

	if (existsSync(resolve(rootPath, 'docs', 'libs', lang))) {
		const libs = readdirSync(resolve(rootPath, 'docs', 'libs', lang));
		if (libs.length) {
			for (const libName of libs) {
				if (!libName.endsWith('.md') && libName !== '.DS_Store') {
					const pathLib = resolve(rootPath, 'docs', 'libs', lang, libName);
					libs_nav.push({
						text: libName,
						collapsed: true,
						items: getDirNav(`/libs/${lang}/${libName}`, pathLib),
					});
				}
			}
		}
	}
	return libs_nav;
};

// https://vitepress.dev/reference/site-config
export default defineConfig({
	title: 'Nebula Lambda',
	description: 'Documentation of this project.',
	themeConfig: {
		// https://vitepress.dev/reference/default-theme-config
		nav: [
			{
				text: 'Introducción',
				link: '/introduction',
			},
			{
				text: 'Cloud Architecture',
				link: '/cloud-architecture',
			},
		],

		sidebar: [
			{
				items: [
					{
						text: 'Introducción',
						link: '/introduction',
					},
					{
						text: 'Cloud Architecture',
						link: '/cloud-architecture',
					},
					{
						text: 'OpenAPI - Swagger UI',
						link: '/openapi',
					},
				],
			},
			{
				text: 'Lambda Functions',
				items: [
					{
						text: 'Introduction',
						link: '/apps/',
					},
					...getAppSidebarItems(),
				],
			},
			{
				text: 'Libraries',
				items: [
					{
						text: 'Introduction',
						link: '/libs/',
					},
					{
						text: 'Libraries TypeScript',
						collapsed: true,
						items: [
							{
								text: 'Introduction',
								link: '/libs/ts/',
							},
							...getLibSidebarItems('ts'),
						],
					},
					{
						text: 'Libraries Python',
						collapsed: true,
						items: [
							{
								text: 'Introduction',
								link: '/libs/py/',
							},
							...getLibSidebarItems('py'),
						],
					},
					{
						text: 'Libraries Go',
						collapsed: true,
						items: [
							{
								text: 'Introduction',
								link: '/libs/go/',
							},
							...getLibSidebarItems('go'),
						],
					},
				],
			},
		],

		socialLinks: [
			{
				icon: {
					svg: '<svg role="img" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title>GitLab</title><path d="m23.6004 9.5927-.0337-.0862L20.3.9814a.851.851 0 0 0-.3362-.405.8748.8748 0 0 0-.9997.0539.8748.8748 0 0 0-.29.4399l-2.2055 6.748H7.5375l-2.2057-6.748a.8573.8573 0 0 0-.29-.4412.8748.8748 0 0 0-.9997-.0537.8585.8585 0 0 0-.3362.4049L.4332 9.5015l-.0325.0862a6.0657 6.0657 0 0 0 2.0119 7.0105l.0113.0087.03.0213 4.976 3.7264 2.462 1.8633 1.4995 1.1321a1.0085 1.0085 0 0 0 1.2197 0l1.4995-1.1321 2.4619-1.8633 5.006-3.7489.0125-.01a6.0682 6.0682 0 0 0 2.0094-7.003z"/></svg>',
				},
				link: 'https://gitlab.com/xavicode/nebula-lambda',
			},
		],
	},
	vite: {
		plugins: [
			SearchPlugin({
				previewLength: 62,
				buttonLabel: 'Search',
				placeholder: 'Search docs',
				allow: [],
				ignore: [],
			}),
		],
	},
});
