---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: Nebula Lambda
  text: Documentation of this project.
  tagline: Documentation of this project.
  actions:
    - theme: brand
      text: Getting Started
      link: /introduction
    - theme: brand-alt
      text: Cloud Architecture
      link: /cloud-architecture

features:
  - title: Feature A
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature B
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
  - title: Feature C
    details: Lorem ipsum dolor sit amet, consectetur adipiscing elit
---

